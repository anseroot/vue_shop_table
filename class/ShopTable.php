<?php
class ShopTable {
	var $outArray = array();
	var $link = '';
	private $orderUserKeyArr = array(
			'sid',
			'fake_id',
			'data',
			'summ',
			'fio',
			'status',
			'adress',
			'payType',
			'delivery',
			'phone',
			'email'
		);
	private $orderGoodsKeyArr = array(
		'list_id',
		'title',
		'count',
		'price'
	);
	
	function __construct(){
		$this->link = new mysqli("127.0.0.1", "root", "", "test");
	}
	
	function closeLink(){
		$this->link->close();
	}
	
	
	function makeJson(){
		return json_encode($this->outArray);
	}
	
	function getUserField($key){
//		 >= 0 затягивают false из-за =0, надо погуглить
		if (array_search($key, $this->orderUserKeyArr) === false) {
			return false;
		} else {
			return true;
		}
	}
	
	function getGoodsField($key){
		if (array_search($key, $this->orderGoodsKeyArr) === false) {
			return false;
		} else {
			return true;
		}
	}
	
	private function getUserFieldForSql(){
		$out = array();
		foreach($this->orderUserKeyArr as $value){
			if ($value === 'sid') {$value = 'id as sid';}
			$out[] = 'tab.'.$value;
		}
		
		$out = implode(',',$out);
		return $out;
	}
	private function getGoodsFieldForSql(){
		$out = array();
		foreach($this->orderGoodsKeyArr as $value){
			if ($value === 'list_id') {$value = 'id as list_id';}
			$out[] = 'ord.'.$value;
		}
		$out = implode(',',$out);
		return $out;
	}
	
	function getShopOrder(){
		$this->outArray['ShopOrder'] = array();
		
		//посмотреть на возможности подготовленного запроса mysqli
		$result = $this->link->query('SELECT '.$this->getUserFieldForSql().','.$this->getGoodsFieldForSql().' FROM vue_shop_table AS tab JOIN vue_shop_orders AS ord ON tab.id = ord.order_id ORDER BY tab.id DESC');

		if ($result) {
			$index = -1;
			$iterIndex = 0;
			while ($row = $result->fetch_object()) {
				$temp_arr = (array)$row;
				$index++;
				if ($temp_arr['sid'] !== $this->outArray['ShopOrder'][$iterIndex]['sid']) {
					if ($index != 0) {$iterIndex++;}
					$this->outArray['ShopOrder'][$iterIndex] = array_filter($temp_arr, array($this, 'getUserField'), ARRAY_FILTER_USE_KEY);
					$this->outArray['ShopOrder'][$iterIndex]['goods'][] = array_filter($temp_arr, array($this, 'getGoodsField'), ARRAY_FILTER_USE_KEY);	
				} else {
					$this->outArray['ShopOrder'][$iterIndex]['goods'][] = array_filter($temp_arr, array($this, 'getGoodsField'), ARRAY_FILTER_USE_KEY);
				}
					
			}
		} else {
			 $this->outArray['ShopOrder'] = array();
		}
//		echo('<pre>');
//		var_dump($this->outArray['ShopOrder']);
//		die();

	}
	
	
	function statusList(){
		$this->outArray['StatusList'] = array(
			'new',
			'old',
			'work'
		);
	}
}
?>