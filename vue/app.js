Vue.component('goods', {
	props: ['goods'],
	template: `<div>
		<div v-for="product in goods" class="df">
			<div>{{product.title}}</div>
			<div>{{product.count}}</div>
			<div>{{product.price}}</div>
		</div>
	</div>`
});
Vue.component('modal', {
	template: '#modal_order_view',
	props: ['order']
});

Vue.filter('moneyView', function (value) {
	value = (value+'').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
	return value;
});
var IdDesc = function (d1, d2) {
	return d1.id < d2.id;
}

function format_time(date) {
	if (date < 10) {
		date = '0' + date;
	}
	return date;
}

function getDateAgo(date, days) {
  var dateCopy = new Date(date);

  dateCopy.setDate(date.getDate() - days);
  return dateCopy;
}
function strVsStr(search_str, full_str){
	if (full_str.toLowerCase().indexOf(search_str.toLowerCase()) > -1 ) {
		return true;
	} else {
		return false;
	}
}

function checkGoods(find_item, arr) {
	return arr.some(function(elem){
		 return strVsStr(find_item, elem.title)
	});
}

function arrayFilterWay(vue_obj) {
	return vue_obj.orders.filter(function (elem) {
		var name,
			adress,
			product,
			status_flag = (vue_obj.filter.status === 'all') ? true: (vue_obj.filter.status === elem.status)? true: false,
			payType,
			delivery,
			phone,
			email,
			id;

		if (elem.data >= vue_obj.filter.time.from && elem.data <= vue_obj.filter.time.to && status_flag) {
			name = (vue_obj.filter.fio !== '') ? strVsStr(vue_obj.filter.fio, elem.name) : true;
			adress = (vue_obj.filter.adress !== '') ? strVsStr(vue_obj.filter.adress, elem.adress) : true;
			product = (vue_obj.filter.product !== '') ? checkGoods(vue_obj.filter.product, elem.goods) : true;
			payType = (vue_obj.filter.payType !== '') ? strVsStr(vue_obj.filter.payType, elem.payType) : true;
			delivery = (vue_obj.filter.delivery !== '') ? strVsStr(vue_obj.filter.delivery, elem.delivery) : true;
			phone = (vue_obj.filter.phone !== '') ? strVsStr(vue_obj.filter.phone, elem.phone) : true;
			email = (vue_obj.filter.email !== '') ? strVsStr(vue_obj.filter.email, elem.email) : true;
			fake_id = (vue_obj.filter.fake_id !== '') ? strVsStr(vue_obj.filter.fake_id, elem.fake_id) : true;
			

			if (vue_obj.filter.id !== ''){
				if (parseInt(vue_obj.filter.id, 10) === elem.id) {
					id = true;
				} else {
					id = false;
				}	
			} else {
				id = true;
			}
			
			if (name && adress && product && payType && delivery && phone && email && id && fake_id) {
				vue_obj.orders_summ += parseInt(elem.summ, 10);
				vue_obj.orders_count += 1;
				return true;
			}
		}
	});
}

var app = new Vue({
	el: '#app',
	data: {
		orders: [],//список заказов
		orders_count: 0,
		orders_summ: 0,
		filter:{
			time: {
				from:'',
				to:''
			},
			fio: '',
			adress: '',
			product: '',
			status: 'all',
			delivery: '',
			payType:'',
			phone:'',
			email:'',
			id:'',
			fake_id:''
		},
		modal_data: null,
		modal_flag: false
	},
	watch:{
		orders:function(){
			console.log('asd')
		}	
	},
	computed:{
		filteredOrders: function() {
			this.orders_count = 0;
			this.orders_summ = 0;
			return arrayFilterWay(this).sort(IdDesc);
		}
	},
	mounted: function () {
//		устанавливаем дату по умолчанию, два месяца от текущего дня, 
//			потом добавить дату к запросу на получение списка закозов
		var time_now = new Date();
		var time_from = getDateAgo(time_now, 60);
		this.filter.time.to = time_now.getFullYear() + '-' + format_time(time_now.getMonth() + 1) + '-' +  format_time(time_now.getDate());
		this.filter.time.from = time_from.getFullYear() + '-' + format_time(time_from.getMonth() + 1) + '-' +  format_time(time_from.getDate());
		
//		запрос на получение заказов
		var url = 'phpApp/getShopOrder.php';
		axios.get(url).then(response => {
			this.orders = response.data.ShopOrder;
			this.keyForFind = response.data.keyForFind;
			this.StatusList = response.data.StatusList;
		});
	},
	methods:{
		changeStatus: function (item){
			this.filter.status = item;
		},
		setActiveStatus: function (item){
			if (this.filter.status === item) {
				return 'selected';
			} else {
				return '';
			}
		}
	}
});